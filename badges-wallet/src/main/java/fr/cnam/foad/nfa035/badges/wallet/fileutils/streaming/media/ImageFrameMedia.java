package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.IOException;
import java.io.OutputStream;

public interface ImageFrameMedia<T> {
    /**
     * {@inheritDoc}
     * @return
     */
    public T getChannel() ;

    /**
     * Permet d'obtenir le flux d'écriture sous-tendant à notre canal
     *
     * @return le flux d'écriture
     * @throws IOException
     */
    public abstract OutputStream getEncodedImageOutput() throws IOException;
}
